// Import built-in module readline
const readline = require('readline');
const rl = readline.createInterface({
    // create instance of readline class
    input: process.stdin,
    output: process.stdout,
})
//Color for command line text
const COLOR_GREEN = "\x1b[32m%s\x1b[0m";
const COLOR_YELLOW = "\x1b[33m%s\x1b[0m";
const COLOR_RED = "\x1b[31m%s\x1b[0m";

class Calculator {
    //Create Calculator Class
    static VERSION = "v1.0";
    static PHI = 3.14;
    static DESCRIPTION = "Selamat Datang Di Kalkulator v1.0";
    static AUTHOR = "rizkysr90";

    constructor() {
        this.menu = {
            "1" : "Penjumlahan",
            "2" : "Pengurangan",
            "3" : "Perkalian",
            "4" : "Pembagian",
            "5" : "Akar Kuadrat",
            "6" : "Luas Persegi",
            "7" : "Volume Kubus",
            "8" : "Volume Tabung",
            "0" : "Keluar"
        }
    }
    get getMenu() {
        //method for get all menu from object menu properties and display them to the console
        console.log(COLOR_GREEN,"==============================================");
        console.log(COLOR_GREEN,Calculator.DESCRIPTION);
        console.log(COLOR_GREEN,"Pilih operasi dibawah ini :");
        console.log(COLOR_GREEN,"==============================================");
        
        for (const key in this.menu) {
            if (key === "0") {
                continue;
            } else {
                console.log(COLOR_YELLOW,`${key}. ${this.menu[key]}`);
            }
        }
        console.log(COLOR_RED,"\n0. KELUAR");
        console.log("==============================================");
    }
    plus(firstOperand,secondOperand) {
        return firstOperand + secondOperand;
    }
    subtract(firstOperand,secondOperand) {
        return firstOperand - secondOperand;
    }
    multiply(firstOperand,secondOperand){
        return firstOperand * secondOperand;
    }
    divide(firstOperand,secondOperand){
        return firstOperand / secondOperand;
    }
    squareRoot(bilangan,pemangkat){
        return bilangan ** pemangkat;
    }
    squareArea(sisi) {
        return this.squareRoot(sisi,2);
    }
    cubeVolume(rusuk) {
        return this.squareRoot(rusuk,3);
    }
    tubeVolume(r,t){
        return Calculator.PHI * this.squareRoot(r,2) * t;
    }
}

class ErrorMessageCalculator{
    //Create ErrorMessageCalculator for define an error
    constructor(code,description,message) {
        this.code = code
        this.description = description;
        this.message = message;
    }
    set setError(code) {
        // method setter for set properties in this class
        const format = (code,description,message) => {
            //utility function for set properties in this class
            this.code = code
            this.description = description;
            this.message = message
        }
        if (code === "001") {
            format("001","Invalid Input","Mohon masukkan input hanya bilangan saja!")
        } else if (code === "002") {
            format("002","Operator Not Found","Operator yang anda pilih tidak ditemukan")
        }
    }
    get getError() {
        //method getter for display an error message in console
        console.log(COLOR_RED,"==============================================");
        console.log(COLOR_RED,"ERROR MESSAGE");
        console.log(COLOR_RED,"==============================================");
        console.log(`Kode : ${this.code}\nDeskripsi : ${this.description}\nPesan : ${this.message}`);
    }
}

const startCalculator = () => {
    // Function for start the calculator
    let firstInput,secondInput,result,operator;
    const handleExit = (userInput) => {
        // Callback function for deciding is Calculator still running or not 
        userInput = userInput.toLowerCase();
        if (userInput === "y") {
            // if true,do a recursive call (recursive function)
            startCalculator();
        } else {
            rl.close();
        }
    }
    const getFirstInput = userInput => {
        // Callback function for process the first input from console 
        firstInput = Number(userInput);
        if (isNaN(firstInput)) {
             //Handle Invalid Input and show the error
            errorMessage.setError = "001";
            errorMessage.getError;
            // Close console
            rl.close();
        } else {
            // Get second input from console
            if (operator === "0") {   
                rl.close();
            } else if ( operator === "1" || operator === "2" || 
                operator === "3" || operator === "4" ) {
                rl.question("Masukkan Angka Kedua : ", getSecondInput);
            } else if (operator === "5") {
                rl.question("Masukkan Pemangkat : ", getSecondInput);
            } else if (operator === "6" || operator === "7") {
                getSecondInput()
            } else if (operator === "8") {
                rl.question("Masukkan Tinggi Tabung (cm) : ", getSecondInput);
            }
        }
    }
    const getSecondInput = userInput => {
        // Callback function for process the second input from console 
        secondInput = Number(userInput);
        if (isNaN(secondInput) && operator !== "6" && operator !== "7") {
           //Handle Invalid Input and show the error
           errorMessage.setError = "001";
           errorMessage.getError;
           // Close console
            rl.close();
        } else {
            // Calculate the result of inputs from console and display it
            switch (operator) {
                case "0" :
                    // Handle Exit from apps Operation
                    rl.close();
                    break;
                case "1" :
                    // Handle Plus Operation
                    result = kalkulator1.plus(firstInput,secondInput);
                    console.log("-------------------------------------------- +");
                    console.log(COLOR_YELLOW,`Hasil Penjumlahan = ${result}`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
                case "2" :
                    // Handle Subtract Operation
                    result = kalkulator1.subtract(firstInput,secondInput);
                    console.log("-------------------------------------------- -");
                    console.log(COLOR_YELLOW,`Hasil Pengurangan = ${result}`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
                case "3":
                    // Handle Multiply Operation
                    result = kalkulator1.multiply(firstInput,secondInput);
                    console.log("-------------------------------------------- *");
                    console.log(COLOR_YELLOW,`Hasil Perkalian = ${result}`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
                case "4":
                    // Handle Divide Operation
                    result = kalkulator1.divide(firstInput,secondInput);
                    console.log("-------------------------------------------- -");
                    console.log(COLOR_YELLOW,`Hasil Pembagian = ${result}`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
                case "5" :
                    // Handle Square Root
                    result = kalkulator1.squareRoot(firstInput,secondInput);
                    console.log("------------------------------------------- **");
                    console.log(COLOR_YELLOW,`Hasil Akar Kuadrat = ${result}`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
                case "6" :
                    result = kalkulator1.squareArea(firstInput);
                    console.log("-------------------------------------------");
                    console.log(COLOR_YELLOW,`Luas Persegi = ${result} cm^2`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
                case "7" :
                    result = kalkulator1.cubeVolume(firstInput);
                    console.log("-------------------------------------------");
                    console.log(COLOR_YELLOW,`Volume Kubus = ${result} cm^3`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
                case "8" :
                    result = kalkulator1.tubeVolume(firstInput,secondInput);
                    console.log("-------------------------------------------");
                    console.log(COLOR_YELLOW,`Volume Tabung = ${result} cm^3`);
                    // Get input from console about,is user want to use the apps again or not?
                    rl.question("Ketik 'Y' atau 'y' untuk melanjutkan perhitungan! atau ketik apa saja untuk keluar : ",handleExit);
                    break;
            }
        }
    }

    const getUserInput = () => {
        rl.question("Kamu pilih : ", (answer) => {
            operator = answer;
            //Handle user input when menu operator is not found
            //Get first input from console
            if (operator === "0") {   
                rl.close();
            } else if ( operator === "1" || operator === "2" || 
                operator === "3" || operator === "4" ) {
                rl.question("Masukkan Angka Pertama : ", getFirstInput);
            } else if(operator === "5") {
                rl.question("Masukkan Angka : ", getFirstInput);
            } else if (operator === "6") {
                rl.question("Masukkan Panjang Sisi Persegi (cm) : ",getFirstInput);
            } else if (operator === "7") {
                rl.question("Masukkan Panjang Rusuk Kubus (cm) : ",getFirstInput);
            } else if (operator === "8") {
                rl.question("Masukkan Panjang Jari-Jari Alas (cm) : ", getFirstInput);
            } else {
                errorMessage.setError = "002";
                errorMessage.getError;
                rl.close();
            }
        })
    }

    kalkulator1.getMenu;
    getUserInput();
   
}

//Create instance kalkulator
let kalkulator1 = new Calculator();
// Create instance for throwing error process while calculator is running
let errorMessage = new ErrorMessageCalculator();
// Let's start the calculator
startCalculator();



rl.on('close',() => console.log(COLOR_YELLOW,`\nTerimakasih,sampai jumpa lagi!👋\nMade with 💖 by ${Calculator.AUTHOR}`))
